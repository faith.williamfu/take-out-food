function parseInputToMap (selectedItems) {
  // need to be implement
  const itemsArr = selectedItems.split(' x ').join().split(',');
  const itemsMap = new Map();
  for (let i = 0; i < itemsArr.length; i = i + 2) {
    itemsMap.set(itemsArr[i], Number(itemsArr[i + 1]));
  }
  return itemsMap;
}

export { parseInputToMap };
