import Promotion from './promotion.js';

class HalfPricePromotion extends Promotion {
  constructor (order) {
    super(order);
    this._type = '指定菜品半价';
    this.halfPriceDishes = ['ITEM0001', 'ITEM0022'];
  }

  get type () {
    return this._type;
  }

  includedHalfPriceDishes () {
    // need to be completed
    const includedHalfPriceDishes = [];
    for (let i = 0; i < this.halfPriceDishes.length; i++) {
      includedHalfPriceDishes.push(this.order.itemsDetails.filter(dishes => dishes.id === this.halfPriceDishes[i]));
    }
    return includedHalfPriceDishes.flat();
  }

  discount () {
    // need to be completed
    let halfPriceDishesAmount = 0;
    halfPriceDishesAmount = this.includedHalfPriceDishes().reduce((acc, cur) => acc + (cur.price * cur.count), 0);
    const discount = halfPriceDishesAmount / 2;
    return discount;
  }
}

export default HalfPricePromotion;
