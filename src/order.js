import dishes from './menu.js';

class Order {
  constructor (itemsMap) {
    // Need to be implement
    this.itemsId = [...itemsMap.keys()];
    this.itemsCount = [...itemsMap.values()];
  }

  get itemsDetails () {
    // Need to be implement
    const itemsDetails = [];
    for (let i = 0; i < this.itemsId.length; i++) {
      itemsDetails.push({ id: this.itemsId[i], name: dishes.filter(item => item.id === this.itemsId[i])[0].name, price: dishes.filter(item => item.id === this.itemsId[i])[0].price, count: this.itemsCount[i] });
    }
    return itemsDetails;
  }

  get totalPrice () {
    // Need to be implement
    let totalPrice = 0;
    totalPrice = this.itemsDetails.reduce((acc, cur) => acc + (cur.price * cur.count), 0);
    return totalPrice;
  }
}

export default Order;
